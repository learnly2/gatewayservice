# Build stage
FROM maven:3.8.3-openjdk-17-slim AS build
ARG APP_ENV=dev
COPY pom.xml /app/
WORKDIR /app
RUN mvn -Dspring.profiles.active=${APP_ENV} dependency:resolve
COPY src/ /app/src/
RUN mvn -Dspring.profiles.active=${APP_ENV} package -DskipTests

# Runtime stage
FROM openjdk:17-jdk-slim
ARG APP_ENV=dev
ENV APP_ENV=${APP_ENV}
ARG JAR_FILE_NAME=gateway-service.jar
ENV JAR_FILE=/app/target/${JAR_FILE_NAME}
COPY --from=build ${JAR_FILE} /app/${JAR_FILE_NAME}
WORKDIR /app
EXPOSE 9347
HEALTHCHECK --interval=30s --timeout=5s --start-period=5s --retries=3 CMD curl --fail http://localhost:9347/api/ph/monitor/health || exit 1
CMD ["java", "-Dspring.profiles.active=${APP_ENV}", "-jar", "/app/${JAR_FILE_NAME}"]
