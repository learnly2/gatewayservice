# Spring Boot Application - README

This README file provides instructions on how to run a ``gatewayService`` stored in GitLab. Follow the steps below to get started.
This service act as a gateway to backend services. Simply routes traffic to backend services by route filtering.

## Prerequisites

Before running the application, ensure that you have the following installed:

- Java Development Kit (JDK) 17 LTS or higher
- Maven build tool
- Git

## Clone the Repository

1. Open a terminal or command prompt.
2. Clone the `https://gitlab.com/learnly2/gatewayservice.git` repository using the `git clone` command:


## Build the Application

1. Navigate to the project directory:
2. Build the application using Maven and execute the following command `mvn -Dspring.profiles.active=dev package`


This command compiles the source code, runs tests, and creates an executable JAR file.

## Run the Application

1. After the build process completes successfully, navigate to target directory `cd target`
2. Run the following command `java -Dspring.profiles.active=dev -jar gateway-service.jar` to run the app
3. The application will start, and you should see logs indicating its progress. The `gatewayService` will run on port `9347` so make sure no other process is running on the same port

**Note** In case you need to `Dockerize` the app run the following command `docker build --build-arg APP_ENV="dev" --tag="ph-gateway-service" .` from the project root directory.

## Access the Application

Once the application is running, you can access it using a web browser or an API testing tool. By default, the application runs on `http://localhost:9347`.

## Configuration
This service does not require a database connection.
If there's need to modify the `route filter` configuration, modify the `application.yml`  file located in the project's resources directory.

**Note**  It's also important to maintain the port number as `9347` as required by the `pharmaclient` front-end client for API calls.

## Conclusion

You have successfully cloned, built, and run the Spring Boot application stored in GitLab. In case of any challenge please reach out.
